# Main
```
client.id -> butler.id(long);
startWaiter@butler;
butler.(id,waiterSpaceID) -> client.waiterSpaceID(long,string);
while running@client do {
	getCommandFromUser@client;
	if closeCommand@client then
		client.(LEAVE,"") -> waiter.command(Request,string);
		waiter.(SUCCESS) -> client.response(Response);
		(running=false)@client;
		kill@waiter;
	else if mapListCommand@client then
		client.(MAP_LIST,"") -> waiter.command(Request,string);
		waiter.(SUCCESS,mapList) -> client.response(Response,string[]);
	else if gameListCommand@client then
		client.(GAME_LIST,"") -> waiter.command(Request,string);
		findRunningGames@waiter
		waiter.(SUCCESS,gameList) -> client.response(Response,string);
	else if joinCommand@client then
		client.(JOIN,gameName) -> waiter.command(Request,string);
		if gameExists(command)@waiter then
			waiter.(SUCCESS,gameSpaceName) -> client.response(Response,string);
			joinGame@client;
			// When the client is done with the game, it will 
			// return to here and repeat the loop.
		else
			waiter.(INVALID_GAME_NAME,"") -> client.response(Response,string);
	else if hostCommand@client then
		client.(HOST,gameName+" "+gameMap) -> waiter.command(Request,string);
		if !validMap(command)@waiter then
			waiter.(INVALID_MAP_NAME) -> client.response(Response,string);
		else if !validGameName(command)@waiter then
			waiter.(INVALID_GAME_NAME) -> client.response(Response,string);
		else
			waiter.(SUCCESS) -> client.response(Response,string);
			// Client can then join the game.
}
```

# Game
## Join game
```
client.(id,JOIN,"") -> host.request(long,Request,string);
if isFull@host then
	host.(id,GAME_FULL) -> client.response(long,Response);
	leave@client;
else if gameStarted@host then
	host.(id,GAME_ALREADY_STARTED) -> client.response(long,Response);
	leave@client;
else
	host.(id,SUCCESS) -> client.response(long,Response);
```

## Leave
```
client.(id,LEAVE,"") -> host.request(long,Request,string);
removeClient(id)@host;
host.(id,SUCCESS) -> client.response(long,Response);
```

## Query roles
```
client.(id,QUERY_ROLES,"") -> host.request(long,Request,string);
host.(id,SUCCESS,currentRoleMap) -> client.response(long,Response,map<string,boolean>);
```

## Choose role
```
client.(id,CHOOSE_ROLE,role) -> host.request(long,Request,string);
if roleDoesNotExist@host then
	host.(id,INVALID_ROLE_NAME) -> client.response(long, Response);
else if roleNotAvailable@host then
	host.(id,ROLE_TAKEN) -> client.response(long,Response);
else
	registerClientRole(id,role)@host;
	host.(id,SUCCESS) -> client.response(long,Response);
```
