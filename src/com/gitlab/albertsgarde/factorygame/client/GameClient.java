package com.gitlab.albertsgarde.factorygame.client;

import java.io.IOException;
import java.io.PrintStream;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Scanner;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.Space;

import com.gitlab.albertsgarde.factorygame.factory.FactoryGameClient;
import com.gitlab.albertsgarde.factorygame.graphics.Gui;
import com.gitlab.albertsgarde.factorygame.server.ChatServer;
import com.gitlab.albertsgarde.factorygame.server.Game;
import com.gitlab.albertsgarde.factorygame.server.GameServer;
import com.gitlab.albertsgarde.factorygame.utils.PSpace;

public class GameClient {
	private final long id;
	
	public GameClient() {
		var random = new SecureRandom();
		id = random.nextLong();
	}
	
	public void run(String url) {
		try {
			// Get private space from lobby.
			Space lobby = null;
			try {
				lobby = PSpace.connect(url, GameServer.LOBBY_SPACE_NAME);
			} catch (IOException e) {
				System.err.println("Could not connect to server at url " + url);
				return;
			} 
			lobby.put(id);
			String privateSpaceID = (String)lobby.get(new ActualField(id), new FormalField(String.class))[1];
			
			System.out.println("Connected to server at url " + url);
			
			// Enter private space.
			Space privateSpace = null;
			try {
				privateSpace = PSpace.connect(url, privateSpaceID);
			} catch (IOException e) {
				System.err.println("Could not connect to server at url " + url);
				return;
			}
			
			
			var scanner = new Scanner(System.in);
			while (handleUserInput(privateSpace, scanner.nextLine(), System.out, scanner, url));
			scanner.close();
			
		} catch (InterruptedException ie) {
			throw new RuntimeException("Thread interrupted.");
		}
		
	}
	
	private boolean handleUserInput(Space space, String input, PrintStream out, Scanner in, String serverURL) throws InterruptedException {
		var inputArray = input.split(" ");
		if (inputArray.length == 0)
			return true;
		var command = inputArray[0];
		var arguments = Arrays.copyOfRange(inputArray, 1, inputArray.length);
		switch (command) {
		case "close":
			if (arguments.length != 0) {
				out.println("Invalid argument '" + arguments[0] + "' for command 'close'.");
				return true;
			}
			space.put(GameServer.Request.LEAVE, "");
			space.get(new ActualField(GameServer.Response.SUCCESS));
			out.println("Disconnecting from server...");
			return false;
		case "mapList":
			if (arguments.length != 0) {
				out.println("Invalid argument '" + arguments[0] + "' for command 'mapList'.");
				return true;
			}
			space.put(GameServer.Request.MAP_LIST, "");
			var mapList = (String[])space.get(new ActualField(GameServer.Response.SUCCESS), new FormalField(String[].class))[1];
			out.println("Available maps:");
			for (var mapName : mapList) {
				out.println(mapName);
			}
			return true;
		case "gameList":
			if (arguments.length != 0) {
				out.println("Invalid argument '" + arguments[0] + "' for command 'gameList'.");
				return true;
			}
			space.put(GameServer.Request.GAME_LIST, "");
			var gameList = (String[])space.get(new ActualField(GameServer.Response.SUCCESS), new FormalField(String[].class))[1];
			out.println("Current games:");
			for (var gameName : gameList) {
				out.println(gameName);
			}
			return true;
		case "join":
			if (arguments.length == 0) {
				out.println("Invalid number of arguments for command 'join'. Please provide the name of the game you wish to join.");
				return true;
			}
			if (arguments.length > 1) {
				out.println("Invalid number of arguments for command 'join'. Command takes only 1 arguments.");
			}
			space.put(GameServer.Request.JOIN_GAME, arguments[0]);
			var response = space.get(new FormalField(GameServer.Response.class), new FormalField(String.class));
			var responseType = (GameServer.Response)response[0];
			switch (responseType) {
			case INVALID_GAME_NAME:
				out.println("No game with the given name '" + arguments[0] + "' are running.");
				return true;
			case SUCCESS:
				var gameSpaceName = (String)response[1];
				out.println("Joining game " + arguments[0] + "...");
				Space gameSpace = null;
				try {
					gameSpace = PSpace.connect(serverURL, gameSpaceName);
				} catch (IOException e) {
					throw new RuntimeException("Could not connect to game space. Space name: " + gameSpaceName);
				}
				joinGame(gameSpace, out, in, serverURL);
				return true;
			default:
				throw new RuntimeException("Cannot handle responses of type: " + responseType);
			}
		case "host":
			if (arguments.length < 2) {
				out.println("Invalid number of arguments for command 'host'. Please provide both the game name and the map name.");
				return true;
			}
			if (arguments.length > 2) {
				out.println("Invalid number of arguments for command 'host'. Command takes only 2 arguments.");
			}
			space.put(GameServer.Request.HOST_GAME, arguments[0] + " " + arguments[1]);
			var response_ = space.get(new FormalField(GameServer.Response.class));
			var responseType_ = (GameServer.Response)response_[0];
			switch (responseType_) {
			case INVALID_GAME_NAME:
				out.println("A game with the given name '" + arguments[0] + "' already exists.");
				return true;
			case INVALID_MAP_NAME:
				out.println("No map with the name '" + arguments[1] + "' exists.");
				return true;
			case SUCCESS:
				out.println("Successfully hosted the game!");
				out.println("You can now join the game through the join <gameName> command.");
				return true;
			default:
				throw new RuntimeException("Cannot handle responses of type: " + responseType_);
			}
		default:
			out.println("Invalid command '" + command + "'.");
			return true;
		}
	}
	
	private void joinGame(Space space, PrintStream out, Scanner in, String serverURL) throws InterruptedException {
		{
			space.put(id, Game.Request.JOIN, "");
			var response = (Game.Response)space.get(new ActualField(id), new FormalField(Game.Response.class))[1];
			if (response == Game.Response.GAME_FULL) {
				out.println("Game full.");
				return;
			} else if (response == Game.Response.GAME_ALREADY_STARTED) { 
				out.println("Game already started.");
				return;
			}
		}
		
		out.println("Successfully joined game!");
		
		var choosingRole = true;
		String role = "";
		while (choosingRole) {
			var userInput = in.nextLine();
			var inputArray = userInput.split(" ");
			if (inputArray.length == 0)
				continue;
			var command = inputArray[0];
			var arguments = Arrays.copyOfRange(inputArray, 1, inputArray.length);
			switch (command) {
			case "leave":
				space.put(id, Game.Request.LEAVE, "");
				space.get(new ActualField(id), new ActualField(Game.Response.SUCCESS));
				return;
			case "roles":
				space.put(id, Game.Request.QUERY_ROLES, "");
				var rolesString = (String)space.get(new ActualField(id), new ActualField(Game.Response.SUCCESS), new FormalField(String.class))[2];
				var roleStringArray = rolesString.split("//");
				for (var roleString : roleStringArray) {
					var roleInfo = roleString.split("/");
					if (roleInfo[1].equals("T")) {
						out.println(roleInfo[0] + ": TAKEN");
					} else {
						out.println(roleInfo[0] + ": FREE");
					}
				}
				break;
			case "chooseRole":
				space.put(id, Game.Request.CHOOSE_ROLE, arguments[0]);
				var response = (Game.Response)space.get(new ActualField(id), new FormalField(Game.Response.class))[1];
				if (response == Game.Response.INVALID_ROLE_NAME) {
					out.println("Role '" + arguments[0] + "' doesn't exist.");
				} else if (response == Game.Response.ROLE_TAKEN) {
					out.println("Role '" + arguments[0] + "' already taken.");
				} else if (response == Game.Response.SUCCESS) {
					out.println("Role '" + arguments[0] + "' is yours!");
					role = arguments[0];
					choosingRole = false;
				}
				break;
			default:
				out.println("Invalid command: " + command);
				break;
			}
		}
		if (role.equals("")) {
			throw new RuntimeException("Sum ting wong.");
		}
		var factorySpacePrefix = (String)space.query(new ActualField(Game.Response.GAME_STARTED), new FormalField(String.class))[1];
		try {
			var gameClient = new FactoryGameClient(serverURL, role, factorySpacePrefix);
			var gui = Gui.createGui(gameClient, gameClient.getInfiniteResources().size() > 0);
			var chatSpaceName = (String)space.query(new ActualField(Game.Response.CHAT_SPACE_NAME), new FormalField(String.class))[1];
			var chatSpace = PSpace.connect(serverURL, chatSpaceName);
			
			out.println("Joining game chat.");
			var guiThread = new Thread(() -> gui.draw(60));
			guiThread.start();
			String userName = null;
			while (userName == null) {
				out.println("Please choose a chat name:");
				var input = in.nextLine();
				if (input == ChatServer.SERVER_USER_NAME) {
					out.println("Invalid chat name.");
				} else {
					userName = input;
				}
			}
			new ChatClient(userName, chatSpace, true, out, in);
			guiThread.join();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Could not connect to server.");
		}
	}
}
