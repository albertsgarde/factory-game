package com.gitlab.albertsgarde.factorygame.client;

import com.gitlab.albertsgarde.factorygame.server.Server;

public class Client {

	public static void main(String[] args) {
		var gameClient = new GameClient();
		gameClient.run(Server.LOCAL_URL);
		return;
	}
}
