package com.gitlab.albertsgarde.factorygame.client;

import java.io.PrintStream;
import java.util.Scanner;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.Space;

import com.gitlab.albertsgarde.factorygame.server.ChatServer;

public class ChatClient {
	private final String userName;
	
	private final Thread thread;
	
	private final Space space;

	private int messageNum;
	
	public ChatClient(String userName, Space space, boolean loadPreviousMessages, PrintStream out, Scanner in) {
		this.userName = userName;
		this.space = space;
		try {
			messageNum = loadPreviousMessages ? 0 : 
				(int) space.query(new ActualField(ChatServer.MessageType.CUR_MESSAGE_NUM), new FormalField(Integer.class))[0];
		} catch (InterruptedException ie) {
			throw new RuntimeException("Thread interrupted.");
		}
		thread = new Thread(() -> manageChat(out, in));
		thread.start();
	}
	
	private void manageChat(PrintStream out, Scanner in) {
		var receiveMessagesThread = new Thread(() -> receiveMessages(out));
		receiveMessagesThread.start();
		
		try {
			while (true) {
				space.put(ChatServer.MessageType.MESSAGE, userName, in.nextLine());
			}
		} catch (InterruptedException ie) {
			try {
				space.put(ChatServer.MessageType.LEAVE, userName, "");
			} catch (InterruptedException e) {
				throw new RuntimeException("Thread interrupted.");
			}
			receiveMessagesThread.interrupt();
		}
	}
	
	private void receiveMessages(PrintStream out) {
		try {
			while (true) {
				var messageTuple = space.query(new ActualField(ChatServer.MessageType.MESSAGE), new ActualField(messageNum), 
						new FormalField(String.class), new FormalField(String.class));
				var userName = (String)messageTuple[2];
				var message = (String)messageTuple[3];
				if (userName.equals(this.userName)) {
					
				} else if (userName.equals(ChatServer.SERVER_USER_NAME)) {
					out.println(message);
				} else {
					out.println(userName + ": " + message);
				}
				++messageNum;
			}
		} catch (InterruptedException ie) {
			
		}
	}
}
