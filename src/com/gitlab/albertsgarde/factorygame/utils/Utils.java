package com.gitlab.albertsgarde.factorygame.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Utils {
	public static <T> List<T> toOwned(Iterable<T> iterable) {
		var list = new ArrayList<T>();
		for (var t : iterable)
			list.add(t);
		return list;
	}
	
	public static <K, V> Map<K, V> toOwned(Map<K, V> map) {
		var result = new LinkedHashMap<K, V>();
		for (var entry : map.entrySet())
			result.put(entry.getKey(), entry.getValue());
		return result;
	}
	
	public static <T> Set<T> toOwnedSet(Iterable<T> iterable) {
		var result = new LinkedHashSet<T>();
		for (var t : iterable) {
			result.add(t);
		}
		return result;
	}
	
	public static <T> String toString(Iterable<T> iterable) {
		var result = "[";
		for (var t : iterable) {
			result += t.toString() + ",";
		}
		return result.substring(0, result.length() - 1) + "]";
	}
	
	public static <K, V> String toString(Map<K, V> map) {
		var result = "{";
		for (var kv : map.entrySet()) {
			result += kv.getKey().toString() + ": " + kv.getValue().toString() + ", ";
		}
		return result.substring(0, result.length() - 2) + "}";
	}
}
