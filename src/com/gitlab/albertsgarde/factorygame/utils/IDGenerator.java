package com.gitlab.albertsgarde.factorygame.utils;

import java.security.SecureRandom;
import java.util.Random;

public class IDGenerator {
	private final Random random;
	
	private static final char[] symbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ12345678890".toCharArray();
	
	public IDGenerator() {
		random = new SecureRandom();
	}
	
	public String generateID(int length) {
		if (length < 1)
			throw new IllegalArgumentException("Length must be greater than 0! Length: " + length);
		char[] buffer = new char[length];
		for (int i = 0; i < length; ++i)
			buffer[i] = symbols[random.nextInt(symbols.length)];
		return new String(buffer);
	}
}