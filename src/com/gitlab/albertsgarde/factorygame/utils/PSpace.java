package com.gitlab.albertsgarde.factorygame.utils;

import java.io.IOException;
import java.net.UnknownHostException;

import org.jspace.RemoteSpace;

public class PSpace {
	public static RemoteSpace connect(String url, String spaceName) throws UnknownHostException, IOException {
		return new RemoteSpace(url + "/" + spaceName + "?keep");
	}
}
