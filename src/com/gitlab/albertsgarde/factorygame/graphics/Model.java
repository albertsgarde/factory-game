package com.gitlab.albertsgarde.factorygame.graphics;

import java.awt.Color;
import java.io.IOException;

import com.gitlab.albertsgarde.factorygame.factory.InvalidFactoryFileException;
import com.google.gson.stream.JsonReader;

/**
 * Represents a graphical model.
 */
public class Model {
	public static enum Type {
		CIRCLE,
		SQUARE,
		TRIANGLE,
		DIAMOND,
	}
	
	private final Type type;
	private final Color color;
	
	public Model(Type type, Color color) {
		this.type = type;
		this.color = color;
	}
	
	/**
	 * Draws the model so that the center of the model is at the given coordinates
	 * @param x - The x-coordinate of the center of the model.
	 * @param y - The y-coordinate of the center of the model.
	 */
	public void draw(double x, double y, int radius) {
		StdDraw.setPenColor(color);
		switch (type) {
			case CIRCLE:
				StdDraw.filledCircle(x, y, radius);
				break;
			case SQUARE:
				StdDraw.filledSquare(x, y, radius);
				break;
			case TRIANGLE:
				double[] yarray = {y+radius,y-radius,y-radius};
				double[] xarray = {x,x+radius,x-radius};
				StdDraw.filledPolygon(xarray, yarray);
				break;
			case DIAMOND:
				double[] yarrayd = {y+radius,y,y-radius,y};
				double[] xarrayd = {x,x+radius,x, x-radius};
				StdDraw.filledPolygon(xarrayd, yarrayd);
				break;
		}
	}
	
	public static Model fromJson(JsonReader jsonReader) throws IOException, InvalidFactoryFileException {
		jsonReader.beginObject();
		Color color = null;
		Type type = null;
		while (jsonReader.hasNext()) {
			var key = jsonReader.nextName();
			if (key.equals("color")) {
				if (color != null)
					throw new InvalidFactoryFileException("Multiple colors defined for model.");
				else {
					jsonReader.beginArray();
					var red = jsonReader.nextInt();
					var green = jsonReader.nextInt();
					var blue = jsonReader.nextInt();
					jsonReader.endArray();
					color = new Color(red, green, blue);
				}
			} else if (key.equals("type")) {
				if (type != null)
					throw new InvalidFactoryFileException("Multiple types specified for model.");
				else {
					var typeString = jsonReader.nextString();
					try {
						type = Type.valueOf(typeString.toUpperCase());
					} catch (IllegalArgumentException iae) {
						throw new InvalidFactoryFileException("Invalid type for model: '" + typeString + "'.");
					}
				}
			} else {
				throw new InvalidFactoryFileException("Invalid field for model: '" + key + "'.");
			}
		}
		jsonReader.endObject();
		if (color == null)
			throw new InvalidFactoryFileException("Color not specified for model.");
		if (type == null)
			throw new InvalidFactoryFileException("Type not specified for model.");
		return new Model(type, color);
	}
}
