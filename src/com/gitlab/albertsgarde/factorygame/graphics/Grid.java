package com.gitlab.albertsgarde.factorygame.graphics;

public class Grid {
	private int x;
	private int y;
	private int centerX;
	private int centerY;
	private int boxRadius;
	private int boxSize;
	private int halfWidth;
	private int halfHeight;
	private int leftX;
	private int topY;
	
	/**
	 * 
	 * @param centerX
	 * @param centerY
	 * @param x - Number of boxes in x direction
	 * @param number of boxes in y direction
	 * @param boxSize
	 */
	public Grid(int centerX, int centerY, int x, int y, int boxSize) {
		this.centerX=centerX;
		this.centerY=centerY;
		this.x=x;
		this.y=y;
		this.boxSize=boxSize;
		this.boxRadius = boxSize/2;
		this.halfWidth = x*boxRadius;
		this.halfHeight = y*boxRadius;
		this.leftX = centerX - halfWidth + boxRadius;
		this.topY = centerY + halfHeight - boxRadius;
	}
	
	public int getX() {
		return x;
	}
	
	public void draw() {
		StdDraw.setPenColor(StdDraw.LIGHT_GRAY);
		StdDraw.filledRectangle(centerX, centerY, halfWidth, halfHeight);
	}
	
	/**
	 * 
	 * @param place - Counting from 0
	 * @return x-coords for center of the element at the given place in canvas-coordinates
	 */
	public int getElementCenterX(int place){
		return leftX+boxSize*(place % x);
	}
	
	/**
	 * 
	 * @param place - Counting from 0
	 * @return y-coords for center of the element at the given place in canvas-coordinates
	 */
	public int getElementCenterY(int place){
		return topY - boxSize * (place / x);
	}
	
	/**
	 * 
	 * @return position in grid
	 */
	public int getPlace() {
		int mousex = (int)StdDraw.mouseX();
		int mousey = (int)StdDraw.mouseY();
		//System.out.println((mousex-50) + "," + (mousey-45));
		int left = centerX-halfWidth;
		int right = centerX + halfWidth;
		int top = centerY+ halfHeight;
		int bot = centerY - halfHeight;
		
		
		if (mousex<right && mousex>left && mousey>bot && mousey<top) {
			int xbox = (mousex-left)/boxSize;
			int ybox = (mousey-bot)/boxSize;
			
			int place = xbox+((y-1)-ybox)*x;
			//System.out.println("box: " + place);
			return place;
		}
		else {
			return -1;
		}
	}
	
	public int getPlace(int mousex, int mousey) {
		//System.out.println((mousex-50) + "," + (mousey-45));
		int left = centerX-halfWidth;
		int right = centerX + halfWidth;
		int top = centerY+ halfHeight;
		int bot = centerY - halfHeight;
		
		
		if (mousex<right && mousex>left && mousey>bot && mousey<top) {
			int xbox = (mousex-left)/boxSize;
			int ybox = (mousey-bot)/boxSize;
			
			int place = xbox+((y-1)-ybox)*x;
			//System.out.println("box: " + place);
			return place;
		}
		else {
			return -1;
		}
	}

}
