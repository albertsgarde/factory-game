package com.gitlab.albertsgarde.factorygame.graphics;

import java.awt.Font;
import java.awt.event.MouseEvent;

import com.gitlab.albertsgarde.factorygame.factory.FactoryGameClient;
import com.gitlab.albertsgarde.factorygame.factory.Recipe;
import com.gitlab.albertsgarde.factorygame.factory.Resource;

public class Gui {
	
	private final int boxsize;// = 50;
	private final Grid resourceGrid;// = new Grid(250,270,8,9,50);
	private final Grid activeGrid;// = new Grid(720,470,8,1,50);
	private final Grid recipeGrid;// = new Grid(720,195,8,6,50);
	private final Grid infResourceGrid;
	private final FactoryGameClient client;
	
	/**
	 * Makes the gui and draws the initial scene.
	 * @param client
	 * @param boxsize
	 * @param resourceGrid
	 * @param activeGrid
	 * @param recipeGrid
	 */
	
	/*
	public static void main(String[] args) {
		FactoryGameClient client = new FactoryGameClient();
		
		Gui gui = createGui(client, true);
		gui.draw(60);
	}	
	*/
	
	public static Gui createGui(FactoryGameClient client, boolean withInfRes) {
		if (withInfRes) {
			var infResources = client.getInfiniteResources();
			return new Gui(client, 50, new Grid(250,370,8,7,50),new Grid(720,520,8,1,50),new Grid(720,220,8,7,50),new Grid(250,70,infResources.size(),1,50));
		}
		else {
			return new Gui(client, 50, new Grid(250,295,8,10,50),new Grid(720,520,8,1,50),new Grid(720,220,8,7,50));
		}
	}
	
	public Gui(FactoryGameClient client, int boxsize, Grid resourceGrid, Grid activeGrid, Grid recipeGrid) {
		this.resourceGrid=resourceGrid;
		this.activeGrid = activeGrid;
		this.recipeGrid=recipeGrid;
		this.boxsize = boxsize;
		this.client=client;
		this.infResourceGrid = new Grid(2000,0,1,1,0);
		
		StdDraw.show();
		drawCanvas();
		drawScene();
		drawAllResources();
		drawRecipes();
		StdDraw.show();
	}
	
	public Gui(FactoryGameClient client, int boxsize, Grid resourceGrid, Grid activeGrid, Grid recipeGrid , Grid infResourceGrid) {
		this.resourceGrid=resourceGrid;
		this.activeGrid = activeGrid;
		this.recipeGrid=recipeGrid;
		this.boxsize = boxsize;
		this.client=client;
		this.infResourceGrid = infResourceGrid;
		
		StdDraw.show();
		drawCanvas();
		drawScene();
		infResourceGrid.draw();
		StdDraw.text(250, 125, "Infinite Resources");
		drawAllResources();
		drawRecipes();
		StdDraw.show();
	}
	
	
	private void tick() {
		
			StdDraw.show(0);
			
			drawScene();
			colorBox(resourceGrid);
			colorBox(activeGrid);
			colorBox(infResourceGrid);
			drawAllResources();
			drawInfResources();
			java.util.List<MouseEvent> clicks = StdDraw.getNewMouseEvents();
			clickResource(clicks);
			clickInfResource(clicks);
			StdDraw.show(0);
		
	}
	
	/**
	 * Draws and update the gui forever with the given fps
	 * @param fps
	 */
	public void draw(int fps) {
	    long now;
	    long updateTime;
	    long wait;

	    final int TARGET_FPS = fps;
	    final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;

	    while (true) {
	        now = System.nanoTime();

	        tick();

	        updateTime = System.nanoTime() - now;
	        wait = (OPTIMAL_TIME - updateTime) / 1000000;

	        try {
	        	if (wait>=0) {
	            Thread.sleep(wait);
	        	}
	        } catch (Exception e) {
	            e.printStackTrace();
	        }


	    }
	}
	
	private void drawCanvas() {
		StdDraw.setCanvasSize(1000, 650);
		StdDraw.setXscale(0, 999);
		StdDraw.setYscale(0, 649);
		StdDraw.clear(StdDraw.DARK_GRAY);
		
		StdDraw.setPenColor(StdDraw.LIGHT_GRAY);
		Font font = new Font("Arial", Font.BOLD, 50);
		StdDraw.setFont(font);
		StdDraw.text(250, 580, "Resources");
		StdDraw.text(720, 580, "Active Resources");
		recipeGrid.draw();
		StdDraw.text(720, 425, "Recipes");
	}
	
	private void drawScene() {
		//Resource box
		resourceGrid.draw();
		//Active resources box
		activeGrid.draw();
		infResourceGrid.draw();
		
	}
	
	private int drawResources(Grid grid, int amount, Model model, int count) {
		for (int i=0; i < amount; i++) {
			int placex = grid.getElementCenterX(count);
			int placey = grid.getElementCenterY(count);
			model.draw(placex, placey,15);
			count ++;
		}
		return count;
	}
	
	private void drawAllResources() {
		int count = 0;
		var myResources = client.getStore();
		for (var resource : myResources.entrySet()) {
			count = drawResources(resourceGrid, resource.getValue(), resource.getKey().getModel(),count);
		}
		
		count = 0;
		var activeResources = client.getStagingArea();
		for (var resource : activeResources.entrySet()) {
			count = drawResources(activeGrid, resource.getValue(), resource.getKey().getModel(),count);
		}
	}
	
	private void drawInfResources() {
		int count = 0;
		int placex = 0;
		int placey = 0;
		var initeResources = client.getInfiniteResources();
		for (var resource : initeResources) {
			placex = infResourceGrid.getElementCenterX(count);
			placey = infResourceGrid.getElementCenterY(count);
			resource.getModel().draw(placex, placey,15);
			count ++;
		}
	}
	
	private void drawRecipes() {
		int count = 0;
		var recipes = client.getRecipes();
		for (var recipe : recipes) {
			count = drawRecipe(recipe, count);
			count=count/recipeGrid.getX()*recipeGrid.getX()+recipeGrid.getX();
		}
	}
	
	private int drawRecipe(Recipe recipe, int count) {
		var inputs = recipe.getInputs();
		var outputs = recipe.getOutputs();
		int amount = 0;
		int placex;
		int placey;
		Resource resource;
		for (var input : inputs.entrySet()) {
			amount = input.getValue();
			resource = input.getKey();
			for (int i=0; i < amount; i++) {
				placex = recipeGrid.getElementCenterX(count);
				placey = recipeGrid.getElementCenterY(count);
				resource.draw(placex, placey,15);
				count ++;
			}
		}
		placex = recipeGrid.getElementCenterX(count);
		placey = recipeGrid.getElementCenterY(count);
		placey-=5;
		StdDraw.setPenColor(StdDraw.DARK_GRAY);
		StdDraw.text(placex, placey, "=");
		count++;
		
		for (var output : outputs.entrySet()) {
			amount = output.getValue();
			resource = output.getKey();
			for (int i=0; i < amount; i++) {
				placex = recipeGrid.getElementCenterX(count);
				placey = recipeGrid.getElementCenterY(count);
				resource.draw(placex, placey,15);
				count ++;
			}
		}
		
		return count;
	}
	
	private void clickInfResource(java.util.List<MouseEvent> clicks) {
		int place;
		int count=0;
		var myResources = client.getInfiniteResources();
		for (var click : clicks) {
			if(click.getID() == MouseEvent.MOUSE_RELEASED) {
				int mouseX = (int) StdDraw.userX(click.getX());
				int mouseY = (int) StdDraw.userY(click.getY());
				place = infResourceGrid.getPlace(mouseX,mouseY);
				for (var resource : myResources) {
					if (place==count) {
						client.stageResource(resource);
						break;
					}
					else {
						count++;
					}
				}
			}
		}
	}
	
	private void clickResource(java.util.List<MouseEvent> clicks) {
		int mouseX;
		int mouseY;
		int place;
		int count=0;
		int amount=0;
		int action=0;
		var myResources = client.getStore();
		Resource clickedResource;
		boolean done = false;
		for (var click : clicks) {
			if(click.getID() == MouseEvent.MOUSE_RELEASED) {
				mouseX = (int) StdDraw.userX(click.getX());
				mouseY = (int) StdDraw.userY(click.getY());
				place = resourceGrid.getPlace(mouseX,mouseY);
				if (place==-1) {
					place = activeGrid.getPlace(mouseX,mouseY);
					if (place==-1) {
						continue;
					}
					action=1;
				}
				count=0;
				done=false;
				
				if (action==1) {
					myResources = client.getStagingArea();
				}
				for (var resource : myResources.entrySet()) {
					if (done) {
						break;
					}
					amount = resource.getValue();
					clickedResource = resource.getKey();
					for (int i=0;i<amount;i++) {
						if (place==count) {
							switch(action) {
							case 0:
								client.stageResource(clickedResource);
								break;
							case 1:
								client.unstageResource(clickedResource);
								break;
						}
							done=true;
							break;
						}
						else {
							count++;
						}
					}
				}
			}
		}
	}
	
	private void colorBox(Grid grid) {
		int mousePlace = grid.getPlace();
		if (mousePlace==-1) {
			return;
		}
		else {
			StdDraw.setPenColor(StdDraw.GRAY);
			int centerx = grid.getElementCenterX(mousePlace);
			int centery = grid.getElementCenterY(mousePlace);
			StdDraw.filledSquare(centerx, centery, boxsize/2);
			
			if (StdDraw.mousePressed()) {
				StdDraw.setPenColor(StdDraw.RED);
				StdDraw.square(centerx, centery, boxsize/2);
			}
		}
	}
}


