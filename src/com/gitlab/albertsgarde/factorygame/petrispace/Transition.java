package com.gitlab.albertsgarde.factorygame.petrispace;

import java.util.List;

import com.gitlab.albertsgarde.factorygame.utils.Utils;

public class Transition {
	private final List<PreFlow> preFlows;
	private final List<PostFlow> postFlows;
	
	public Transition(List<PreFlow> preFlows, List<PostFlow> postFlows) {
		this.preFlows = Utils.toOwned(preFlows);
		this.postFlows = Utils.toOwned(postFlows);
	}
	
	public Thread startOnNewThread(PetriSpace space, Runnable action) {
		Thread thread = new Thread(() -> run(space, action));
		thread.start();
		return thread;
	}
	
	public List<PreFlow> getPreFlows() {
		return preFlows;
	}
	
	public List<PostFlow> getPostFlows() {
		return postFlows;
	}
	
	private void run(PetriSpace space, Runnable action) {
			while (true) {
				boolean haveTicket = false;
				long ticket = 0;
				try {
					// Wait for all places to have token at least once.
					for (PreFlow flow : preFlows) {
							flow.query(space);
					}
					// Get the ticket.
					// Make sure that all places still have tokens.
					if (checkAndConsume(space)) {
						// If they do, take those tokens and perform action.
						action.run();
						ticket = space.getTicket();
						haveTicket = true;
						for (PostFlow flow : postFlows) {
							flow.put(space, ticket);
						}
						space.putTicket(ticket);
						haveTicket = false;
					} 
				} catch (InterruptedException e) {
					System.err.println("Thread interrupted.");
					e.printStackTrace();
				} finally {
					try {
						if (haveTicket)
							space.putTicket(ticket);
					} catch (InterruptedException e) {
						throw new RuntimeException("Thread interrupted before TICKET was returned.");
					}
				}
			}
	}
	
	private boolean checkAndConsume(PetriSpace space){
		boolean haveTicket = false;
		long ticket = 0;
		try {
			ticket = space.getTicket();
			haveTicket = true;
			for (PreFlow flow : preFlows) {
				if (!flow.queryp(space))
					return false;
			}
			for (PreFlow flow : preFlows) {
				flow.get(space, ticket);
			}
		} catch (InterruptedException e) {
			throw new RuntimeException("Thread interrupted.");
		} finally {
			try {
				if (haveTicket)
					space.putTicket(ticket);
			} catch (InterruptedException e) {
				throw new RuntimeException("Thread interrupted before TICKET was returned.");
			}
		}
		return true;
	}
}
