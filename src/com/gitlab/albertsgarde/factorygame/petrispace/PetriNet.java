package com.gitlab.albertsgarde.factorygame.petrispace;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.gitlab.albertsgarde.factorygame.utils.Utils;

public final class PetriNet {
	private final Map<String, Transition> transitions;
	
	private final Set<String> places;
	
	public PetriNet(Map<String, Transition> transitions) {
		this.transitions = Utils.toOwned(transitions);
		this.places = new HashSet<>();
		for (var transition : transitions.values()) {
			for (var preFlow : transition.getPreFlows()) {
				places.add(preFlow.getPrePlace());
			}
			for (var postFlow : transition.getPostFlows()) {
				places.add(postFlow.getPostPlace());
			}
		}
	}
	
	public final Transition getTransition(String id) {
		return transitions.get(id);
	}
	
	public final Map<String, Transition> getTransitions() {
		return Collections.unmodifiableMap(transitions);
	}
	
	public final int getNumTransitions() {
		return transitions.size();
	}
	
	public final Set<String> getPlaces() {
		return Collections.unmodifiableSet(places);
	}
	
	public String toJson() {
		var result = "{\"transitions\": {";
		for (var transition : transitions.entrySet()) {
			result += "\"" + transition.getKey() + "\": {\"preFlows\": [";
			for (var preFlow : transition.getValue().getPreFlows()) {
				result += preFlow.toJson() + ", ";
			}
			if (result.endsWith(", "))
				result = result.substring(0, result.length() - 2);
			result += "], \"postFlows\": [";
			for (var postFlow : transition.getValue().getPostFlows()) {
				result += postFlow.toJson() + ", ";
			}
			if (result.endsWith(", "))
				result = result.substring(0, result.length() - 2);
			result += "]}, ";
		}
		if (result.endsWith(", "))
			result = result.substring(0, result.length() - 2);
		return result + "}}";
	}
}
