package com.gitlab.albertsgarde.factorygame.petrispace;

public class PostFlow {
	private final String postPlace;
	private final int amount;
	
	public PostFlow(String postPlace, int amount) {
		this.postPlace = postPlace;
		if (amount < 0)
			throw new IllegalArgumentException("Negative amounts not allowed.");
		this.amount = amount;
	}
	
	final void put(PetriSpace space, long ticket) throws InterruptedException {
		space.addTokens(postPlace, amount, ticket);
	}
	
	final String getPostPlace() {
		return postPlace;
	}
	
	public String toJson() {
		return "{\"postPlace\": \"" + postPlace + "\"}";
	}
}
