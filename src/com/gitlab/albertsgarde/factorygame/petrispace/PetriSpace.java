package com.gitlab.albertsgarde.factorygame.petrispace;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.RemoteSpace;
import org.jspace.SequentialSpace;
import org.jspace.Space;
import org.jspace.SpaceRepository;

public final class PetriSpace {
	
	private static final String MAX = "MAX";
	
	private static final String TICKET = "TICKET";
	
	private static final String URL_CODE = "kcIJOFmqXjpX";
	
	private static final String SETUP_SPACE = "petriSetup" + URL_CODE;
	private static final String PETRI_SPACE = "petri" + URL_CODE;
	
	private final Space space;
	
	private final PetriNet net;
	
	private final SecureRandom random;
	
	public static PetriSpace createPetriSpace(SpaceRepository spaceRepository, PetriNet petriNet, Map<String, Integer> initialMarking, String spacePrefix) {
		
		spaceRepository.add(spacePrefix + SETUP_SPACE, new SequentialSpace());
		Space setupSpace = spaceRepository.get(spacePrefix + SETUP_SPACE);
		try {
			setupSpace.put(petriNet);
		} catch (InterruptedException e1) {
			throw new RuntimeException("Connection interrupted!");
		}
		spaceRepository.add(spacePrefix + PETRI_SPACE, new SequentialSpace());
		Space space = spaceRepository.get(spacePrefix + PETRI_SPACE);
		PetriSpace result = new PetriSpace(space, petriNet);
		try {
			space.put(TICKET);
			var ticket = result.getTicket();
			for (var place : petriNet.getPlaces()) {
				if (!initialMarking.containsKey(place))
					throw new IllegalArgumentException("Initial marking must mark all places. Place missing: " + place);
			}
			for (var place : initialMarking.keySet()) {
				space.put(place, 0);
				space.put(place, 0, MAX);
				result.addTokens(place, initialMarking.get(place), ticket);
			}
			result.putTicket(ticket);
			
		} catch (InterruptedException e) {
			throw new RuntimeException("Connection interrupted!");
		}
		return result;
	}
	
	public static PetriSpace connectToPetriSpace(String uri, String spacePrefix) {
		String setupURL = uri + "/" + spacePrefix + SETUP_SPACE + "?keep";
		Space setupSpace;
		try {
			setupSpace = new RemoteSpace(setupURL);
		} catch (IOException e) {
			throw new RuntimeException("Could not connect to server. URL: " + setupURL);
		} 
		PetriNet net;
		try {
			net = (PetriNet)setupSpace.query(new FormalField(PetriNet.class))[0];
		} catch (InterruptedException e1) {
			throw new RuntimeException("Connection interrupted.");
		}
		String petriURL = uri + "/" + spacePrefix + PETRI_SPACE + "?keep";
		try {
			Space space = new RemoteSpace(petriURL);
			return new PetriSpace(space, net);
		} catch (IOException e) {
			throw new RuntimeException("Could not connect to server. URL: " + petriURL);
		} 
	}
	
	private PetriSpace(Space space, PetriNet net) {
		this.space = space;
		this.net = net;
		this.random = new SecureRandom();
	}
	
	public final Transition getTransition(String transitionID) {
		return net.getTransition(transitionID);
	}
	
	public final Thread startTransitionOnNewThread(String transitionID, Runnable action) {
		return net.getTransition(transitionID).startOnNewThread(this, action);
	}
	
	public final int numTokens(String place) {
		try {
			return (int)space.query(new ActualField(place), new FormalField(Integer.class), new ActualField(MAX))[1];
		} catch (InterruptedException e) {
			throw new RuntimeException("ConnectionInterrupted!");
		}
	}
	
	final long getTicket() throws InterruptedException {
		space.get(new ActualField(TICKET));
		var newTicket = random.nextLong();
		space.put(TICKET, newTicket);
		return newTicket;
	}
	
	final void putTicket(long ticket) throws InterruptedException {
		validateTicket(ticket);
		space.get(new ActualField(TICKET), new ActualField(ticket));
		space.put(TICKET);
	}
	
	final void awaitToken(String place, int amount) throws InterruptedException {
		space.query(new ActualField(place), new ActualField(amount));
	}
	
	final void awaitEmpty(String place) throws InterruptedException {
		space.query(new ActualField(place), new ActualField(0), new ActualField(MAX));
	}
	
	final boolean hasTokens(String place, int amount) throws InterruptedException {
		return space.queryp(new ActualField(place), new ActualField(amount)) != null;
	}
	
	final void addTokens(String place, int amount, long ticket) throws InterruptedException {
		validateTicket(ticket);
		var tokenTuple = space.getp(new ActualField(place), new FormalField(Integer.class), new ActualField(MAX));
		var tokenNum = (int)tokenTuple[1];
		for (var i = 1; i <= amount; ++i) {
			space.put(place, tokenNum+i);
		}
		space.put(place, tokenNum+amount, MAX);
	}
	
	final void removeTokens(String place, int amount, long ticket) throws InterruptedException {
		validateTicket(ticket);
		if (hasTokens(place, amount)) {
			var tokenTuple = space.getp(new ActualField(place), new FormalField(Integer.class), new ActualField(MAX));
			var tokenNum = (int)tokenTuple[1];
			for (var i = 0; i < amount; ++i) {
				if (space.getp(new ActualField(place), new ActualField(tokenNum-i)) == null)
					throw new RuntimeException("Missing tuple.");
			}
			space.put(place, tokenNum-amount, MAX);
		} else {
			throw new IllegalStateException("Can only remove as many tokens as there are. Present tokens: " + 
					numTokens(place) + " Tokens to remove: " + amount);
		}
	}
	
	final void clearTokens(String place, long ticket) throws InterruptedException {
		validateTicket(ticket);
		space.getAll(new ActualField(place), new FormalField(Integer.class));
		space.getAll(new ActualField(place), new FormalField(Integer.class), new ActualField(MAX));
		space.put(place, 0);
		space.put(place, 0, MAX);
	}
	
	final void printMarking() {
		try {
			var ticket = getTicket();
			List<Object[]> marking = space.queryAll(new FormalField(Integer.class));
			System.out.println(Arrays.toString(marking.stream().map((Object[] tuple) -> (int)tuple[0]).toArray()));
			putTicket(ticket);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public final void printPetriNet() {
		System.out.println(net.toString());
	}
	
	public final Map<String, Integer> allTokens() {
		var result = new HashMap<String, Integer>();
		for (var place : net.getPlaces()) {
			result.put(place, numTokens(place));
		}
		return result;
		
	}
	
	private final void validateTicket(long ticket) throws InterruptedException {
		var ticketTuple = space.queryp(new ActualField(TICKET), new FormalField(Long.class));
		if (ticketTuple == null)
			throw new IllegalStateException("Ticket is not taken out.");
		var curTicket = (long)ticketTuple[1];
		if (curTicket != ticket)
			throw new IllegalStateException("Invalid ticket.");
	}
}
