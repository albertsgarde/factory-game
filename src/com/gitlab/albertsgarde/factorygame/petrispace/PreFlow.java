package com.gitlab.albertsgarde.factorygame.petrispace;

public class PreFlow {
	public static enum Type {
		CONSUME,
	}
	
	private final String prePlace;
	private final Type type;
	private final int amount;
	
	public PreFlow(String prePlace, Type type, int amount) {
		this.prePlace = prePlace;
		this.type = type;
		if (amount < 0)
			throw new IllegalArgumentException("Negative amounts not allowed.");
		this.amount = amount;
	}
	
	final void query(PetriSpace space) throws InterruptedException {
		switch (type) {
		case CONSUME:
			space.awaitToken(prePlace, amount);
			break;
		default:
			throw new RuntimeException("Invalid PreFlowType!");
		}
	}
	
	final boolean queryp(PetriSpace space) throws InterruptedException {
		switch (type) {
		case CONSUME:
			return space.hasTokens(prePlace, amount);
		default:
			throw new RuntimeException("Invalid PreFlowType!");
		}
	}
	
	final void get(PetriSpace space, long ticket) throws InterruptedException {
		switch (type) {
		case CONSUME:
			space.removeTokens(prePlace, amount, ticket);
			break;
		default:
			throw new RuntimeException("Invalid PreFlowType!");
		}
	}
	
	final String getPrePlace() {
		return prePlace;
	}
	
	public String toJson() {
		return "{\"prePlace\": \"" + prePlace + "\", \"amount\": " + amount + "}";
	}
}
