package com.gitlab.albertsgarde.factorygame.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentHashMap;

import org.jspace.FormalField;
import org.jspace.SequentialSpace;
import org.jspace.Space;
import org.jspace.SpaceRepository;

import com.gitlab.albertsgarde.factorygame.utils.IDGenerator;

public class GameServer {
	public static enum Request {
		LEAVE,
		MAP_LIST,
		GAME_LIST,
		JOIN_GAME,
		HOST_GAME,
	}
	
	public static enum Response {
		SUCCESS,
		INVALID_GAME_NAME,
		INVALID_MAP_NAME,
		INVALID_ROLE_NAME,
	}
	
	public static final String LOBBY_SPACE_NAME = "lobby";
	
	private final SpaceRepository repository;
	
	private final ConcurrentHashMap<String, String> mapPaths;
	private final String[] mapNames;
	
	private final Thread lobbyManagerThread;
	
	private final ConcurrentHashMap<Long, Thread> clientManagerThreads;
	
	private final ConcurrentHashMap<String, Game> games;
	
	private final IDGenerator idGenerator;
	
	private boolean running;
	
	public GameServer(String url) {
		repository = new SpaceRepository();
		repository.addGate(url + "/?keep");
		repository.add(LOBBY_SPACE_NAME, new SequentialSpace());
		mapPaths = findMapPaths();
		mapNames = mapPaths.keySet().toArray(new String[] {});
		
		lobbyManagerThread = new Thread(() -> manageLobby(repository.get(LOBBY_SPACE_NAME)));
		clientManagerThreads = new ConcurrentHashMap<>();
		games = new ConcurrentHashMap<>();
		idGenerator = new IDGenerator();
		running = false;
	}
	
	public void run() {
		if (running)
			throw new IllegalStateException("Cannot run the same server twice.");
		running = true;
		lobbyManagerThread.start();
	}
	
	private ConcurrentHashMap<String, String> findMapPaths() {
		var result = new ConcurrentHashMap<String, String>();
		try (var paths = Files.walk(Paths.get("assets/maps/"))) {
		    paths
		        .filter(Files::isRegularFile)
		        .filter((var path) -> path.toString().endsWith(".json"))
		        .forEach((var path) -> {var fileName = path.getFileName().toString(); 
		        						result.put(fileName.substring(0, fileName.length()-5), path.toString()); } );
		} catch (IOException ioe) {
			throw new RuntimeException("Could not open maps directory.");
		}
		return result;
	}
	
	private void manageLobby(Space lobbySpace) {
		try {
			while (true) {
				var clientID = (long)lobbySpace.get(new FormalField(Long.class))[0];
				var clientSpaceName = "client-space-" + idGenerator.generateID(12);
				repository.add(clientSpaceName, new SequentialSpace());
				
				var thread = new Thread(() -> manageClient(repository.get(clientSpaceName), clientID));
				if (clientManagerThreads.containsKey(clientID))
					throw new RuntimeException("Duplicate clientID!");
				clientManagerThreads.put(clientID, thread);
				thread.start();
				lobbySpace.put(clientID, clientSpaceName);
			} 
		} catch (InterruptedException e) {
			throw new RuntimeException("Thread interrupted.");
		}
	}
	
	private void manageClient(Space space, long clientID) {
		try {
			boolean running = true;
			while (running) {
				var requestTuple = space.get(new FormalField(Request.class), new FormalField(String.class));
				var request = (Request)requestTuple[0];
				var arguments = (String)requestTuple[1];
				running = handleClientRequest(space, request, arguments.split(" "));
			}
		} catch (InterruptedException e) {
			throw new RuntimeException("Thread interrupted.");
		}
		if (clientManagerThreads.remove(clientID) == null) {
			throw new RuntimeException("Should not be possible.");
		}
	}
	
	private boolean handleClientRequest(Space space, Request request, String[] arguments) throws InterruptedException {
		if (request == Request.LEAVE) {
			space.put(Response.SUCCESS);
			return false;
		} else if (request == Request.MAP_LIST) {
			space.put(Response.SUCCESS, mapNames);
			return true;
		} else if (request == Request.GAME_LIST) {
			var runningGames = games.keySet().toArray(new String[] {});
			space.put(Response.SUCCESS, runningGames);
			return true;
		} else if (request == Request.JOIN_GAME) {
			var gameName = arguments[0];
			if (games.containsKey(gameName)) {
				space.put(Response.SUCCESS, games.get(gameName).getSpaceName());
			} else {
				space.put(Response.INVALID_GAME_NAME, "");
			}
			return true;
		} else if (request == Request.HOST_GAME) {
			var gameName = arguments[0];
			var mapName = arguments[1];
			if (games.containsKey(gameName)) {
				space.put(Response.INVALID_GAME_NAME);
			} else if (!mapPaths.containsKey(mapName)) {
				space.put(Response.INVALID_MAP_NAME);
			} else {
				var game = createGame(gameName, mapName);
				game.start();
				space.put(Response.SUCCESS);
			}
			return true;
		} else {
			throw new RuntimeException("Cannot handle requests of type: " + request);
		}
	}
	
	private Game createGame(String gameName, String mapName) {
		var game = new Game(gameName, mapPaths.get(mapName), this, repository);
		games.put(gameName, game);
		return game;
	}
	
	void removeGame(Game game) {
		games.remove(game.getName());
		repository.remove(game.getSpaceName());
	}
}
