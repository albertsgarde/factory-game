package com.gitlab.albertsgarde.factorygame.server;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.Space;

public final class ChatServer {
	public static enum MessageType {
		LEAVE,
		MESSAGE,
		CUR_MESSAGE_NUM,
	}
	
	public static final String SERVER_USER_NAME = "ADMIN";
	
	private final Thread thread;
	
	private final Space space;
	
	private int messageNum;
	
	public ChatServer(Space space) {
		this.space = space;
		messageNum = 0;
		try {
			space.put(MessageType.CUR_MESSAGE_NUM, messageNum);
		} catch (InterruptedException e) {
			throw new RuntimeException("Thread interrupted.");
		}
		thread = new Thread(() -> manageChat());
		thread.start();
	}
	
	public void close() {
		thread.interrupt();
	}
	
	private void manageChat() {
		try {
			while (true) {
				var messageTuple = space.get(new FormalField(MessageType.class), new FormalField(String.class), 
						new FormalField(String.class));
				var messageType = (MessageType)messageTuple[0];
				var userName = (String)messageTuple[1];
				var message = (String)messageTuple[2];
				if (messageType == MessageType.LEAVE) {
					sendMessage(SERVER_USER_NAME, "User '" + userName + "' has disconnected.");
				} else if (messageType == MessageType.MESSAGE) {
					sendMessage(userName, message);
				} else {
					throw new RuntimeException("Invalid message type from user. User: " + userName + "  MessageType: " + messageType);
				}
			}
		} catch (InterruptedException ie) {
			try {
				sendMessage(SERVER_USER_NAME, "Server closing!");
			} catch (InterruptedException e) {
				throw new RuntimeException("Thread interrupted.");
			}
		}
	}
	
	private void sendMessage(String userName, String message) throws InterruptedException {
		if (space.getp(new ActualField(MessageType.CUR_MESSAGE_NUM), new ActualField(messageNum)) == null) {
			throw new RuntimeException("Sum ting wong.");
		}
		space.put(MessageType.MESSAGE, messageNum, userName, message);
		++messageNum;
		space.put(MessageType.CUR_MESSAGE_NUM, messageNum);
	}
}
