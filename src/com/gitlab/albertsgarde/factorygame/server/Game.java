package com.gitlab.albertsgarde.factorygame.server;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import org.jspace.FormalField;
import org.jspace.SequentialSpace;
import org.jspace.Space;
import org.jspace.SpaceRepository;

import com.gitlab.albertsgarde.factorygame.factory.FactoryGameServer;
import com.gitlab.albertsgarde.factorygame.factory.InvalidFactoryFileException;

public class Game {
	public static enum Request {
		LEAVE,
		JOIN,
		QUERY_ROLES,
		CHOOSE_ROLE,
	}
	
	public static enum Response {
		SUCCESS,
		GAME_FULL,
		INVALID_ROLE_NAME,
		ROLE_TAKEN,
		GAME_STARTED,
		GAME_ALREADY_STARTED,
		CHAT_SPACE_NAME,
	}
	
	private final String name;
	
	private final String spaceName;
	
	private final Space space;
	
	private final String chatSpaceName;
	
	private final Space chatSpace;
	
	private final Thread hostThread;
	
	@SuppressWarnings("unused")
	private final GameServer gameServer;
	
	private final FactoryGameServer factoryGame;
	
	private final int maxPlayers;
	
	private final HashSet<Long> curPlayers;
	
	private final HashMap<Long, String> playerRoles;
	
	
	
	public Game(String name, String mapPath, GameServer gameServer, SpaceRepository repository) {
		this.name = name;
		this.spaceName = "gameSpace" + name;
		repository.add(spaceName, new SequentialSpace());
		this.space = repository.get(spaceName);
		
		this.chatSpaceName = "gameChatSpace" + name;
		repository.add(chatSpaceName, new SequentialSpace());
		this.chatSpace = repository.get(chatSpaceName);
		
		this.hostThread = new Thread(() -> manageGame());
		this.gameServer = gameServer;
		
		try {
			factoryGame = new FactoryGameServer(mapPath, repository, spaceName);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Could not read map file.");
		} catch (InvalidFactoryFileException e) {
			e.printObjectTrace();
			e.printStackTrace();
			throw new RuntimeException("Error reading map file.");
		}
		maxPlayers = factoryGame.getMapDescription().getRoles().size();
		
		curPlayers = new HashSet<>();
		playerRoles = new HashMap<>();
	}
	
	public String getName() {
		return name;
	}
	
	public String getSpaceName() {
		return spaceName;
	}
	
	public void start() {
		hostThread.start();
	}
	
	private void manageGame() {
		var finishedSetup = false;
		while (!finishedSetup) {
			try {
				var requestTuple = space.get(new FormalField(Long.class), new FormalField(Request.class), new FormalField(String.class));
				var clientID = (long)requestTuple[0];
				var request = (Request)requestTuple[1];
				var arg = (String)requestTuple[2];
				finishedSetup = handleClientRequest(clientID, request, arg);
			} catch (InterruptedException ie) {
				throw new RuntimeException("ThreadInterrupted.");
			}
		}
		try {
			space.put(Response.GAME_STARTED, spaceName);
			System.out.println("Game '" + name + "' started!");
		} catch (InterruptedException e) {
			throw new RuntimeException("Thread interrupted.");
		}
		
		var chatServer = new ChatServer(chatSpace);
		try {
			space.put(Response.CHAT_SPACE_NAME, chatSpaceName);
			while (true) {
				var requestTuple = space.get(new FormalField(Long.class), new FormalField(Request.class), new FormalField(String.class));
				var clientID = (long)requestTuple[0];
				var request = (Request)requestTuple[1];
				if (request == Request.JOIN) {
					space.put(clientID, Response.GAME_ALREADY_STARTED);
				} else {
					throw new RuntimeException("Invalid request at current time.");
				}
			}
		} catch (InterruptedException ie) {
			chatServer.close();
			throw new RuntimeException("ThreadInterrupted.");
		}
		
		/*factoryGame.join();
		
		
		gameServer.removeGame(this);*/
	}
	
	private boolean handleClientRequest(long clientID, Request request, String arg) throws InterruptedException {
		if (request == Request.LEAVE) {
			if (!curPlayers.contains(clientID))
				throw new RuntimeException("This should never happen.");
			curPlayers.remove(clientID);
			if (playerRoles.containsKey(clientID)) {
				playerRoles.remove(clientID);
			}
			space.put(clientID, Response.SUCCESS);
		} else if (request == Request.JOIN) {
			if (curPlayers.size() >= maxPlayers)
				space.put(clientID, Response.GAME_FULL);
			else {
				curPlayers.add(clientID);
				space.put(clientID, Response.SUCCESS);
			}
		} else if (request == Request.QUERY_ROLES) {
			var result = "";
			for (var role : factoryGame.getMapDescription().getRoles().keySet()) {
				if (playerRoles.containsValue(role)) {
					result += role + "/T//";
				} else {
					result += role + "/F//";
				}
			}
			space.put(clientID, Response.SUCCESS, result.substring(0, result.length() - 2));
		} else if (request == Request.CHOOSE_ROLE) {
			var requestedRole = arg;
			if (!factoryGame.getMapDescription().getRoles().containsKey(requestedRole)) {
				space.put(clientID, Response.INVALID_ROLE_NAME);
			} else if (playerRoles.containsValue(requestedRole)) {
				space.put(clientID, Response.ROLE_TAKEN);
			} else {
				playerRoles.put(clientID, requestedRole);
				space.put(clientID, Response.SUCCESS);
			}
		}
		return playerRoles.size() == factoryGame.getMapDescription().getRoles().size();
	}
}
