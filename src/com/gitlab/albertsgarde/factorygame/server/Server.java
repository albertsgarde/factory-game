package com.gitlab.albertsgarde.factorygame.server;

public class Server {
	
	public static final String PORT = "15926";
	public static final String LOCAL_URL = "tcp://localhost:" + PORT;

	public static void main(String[] args) {
		var gameServer = new GameServer(LOCAL_URL);
		gameServer.run();
		
		System.out.println("Factory Game server started at " + LOCAL_URL);
		
		return;
	}
}
