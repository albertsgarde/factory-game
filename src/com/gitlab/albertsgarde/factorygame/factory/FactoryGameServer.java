package com.gitlab.albertsgarde.factorygame.factory;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.jspace.FormalField;
import org.jspace.SequentialSpace;
import org.jspace.Space;
import org.jspace.SpaceRepository;

import com.gitlab.albertsgarde.factorygame.petrigame.PetriGameServer;
import com.google.gson.stream.JsonReader;

public final class FactoryGameServer {
	static final String SETUP_SPACE = "factoryGameSetup";
	
	static final String RESOURCE_SPACE = "factoryGameResources";
	
	private final PetriGameServer petriGame;
	
	private final FactoryGameDescription mapDescription;
	
	public FactoryGameServer(String mapFilePath, SpaceRepository repository, String spacePrefix) throws IOException, InvalidFactoryFileException {
		repository.add(spacePrefix + SETUP_SPACE, new SequentialSpace());
		repository.add(spacePrefix + RESOURCE_SPACE,  new SequentialSpace());
		var setupSpace = repository.get(spacePrefix + SETUP_SPACE);
		var mapFileContents = Files.readString(Paths.get(mapFilePath));
		try {
			setupSpace.put(mapFileContents);
			
		} catch (InterruptedException e) {
			throw new RuntimeException("Thread Interrupted.");
		}
		var stringReader = new StringReader(mapFileContents);
		mapDescription = FactoryGameDescription.fromJson(new JsonReader(stringReader));
		
		petriGame = new PetriGameServer(mapDescription.toPetriGame(), repository, spacePrefix);
		
		new Thread(() -> clientManager(repository.get(spacePrefix + RESOURCE_SPACE))).start();
	}
	
	public FactoryGameDescription getMapDescription() {
		return mapDescription;
	}
	
	public void shutdown() {
		petriGame.shutDown();
	}
	
	public void join() {
		petriGame.join();
	}
	
	private void clientManager(Space clientSpace) {
		try {
			while (true) {
				var requestTuple = clientSpace.get(new FormalField(String.class));
				var roleName = (String)requestTuple[0];
				var result = "";
				for (var resourceName : mapDescription.getResources().keySet()) {
					var resourceStoreAmount = petriGame.numTokens(FactoryGameDescription.placeName(resourceName, roleName, false, false));
					var resourceStagedAmount = petriGame.numTokens(FactoryGameDescription.placeName(resourceName, roleName, true, false));
					result += resourceName + ":" + resourceStoreAmount + ":" + resourceStagedAmount + ";"; 
				}
				
				clientSpace.put(roleName, result.subSequence(0, result.length()-1));
			}
		} catch (InterruptedException ie) {
			throw new RuntimeException("Thread interrupted.");
		}
	}
}
