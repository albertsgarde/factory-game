package com.gitlab.albertsgarde.factorygame.factory;

import java.io.IOException;
import java.io.StringReader;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jspace.ActualField;
import org.jspace.FormalField;
import org.jspace.SequentialSpace;
import org.jspace.Space;

import com.gitlab.albertsgarde.factorygame.petrigame.PetriGameClient;
import com.gitlab.albertsgarde.factorygame.utils.PSpace;
import com.google.gson.stream.JsonReader;

public final class FactoryGameClient {
	private final FactoryGameDescription gameDescription;
	
	private final Role role;
	
	private final PetriGameClient petriGameClient;
	
	private final String roleName;
	
	private final Space serverSpace;
	
	private LinkedHashMap<Resource, Integer> store;
	
	private LinkedHashMap<Resource, Integer> stagingArea;
	
	private final Space updateResourcesSpace;
	
	public FactoryGameClient(String url, String roleName, String spacePrefix) throws UnknownHostException, IOException {
		this.roleName = roleName;

		var setupSpace = PSpace.connect(url, spacePrefix + FactoryGameServer.SETUP_SPACE);
		try {
			var gameDescriptionString = (String)setupSpace.queryp(new FormalField(String.class))[0];
			gameDescription = FactoryGameDescription.fromJson(new JsonReader(new StringReader(gameDescriptionString)));
		} catch (InterruptedException e) {
			throw new RuntimeException("Thread interrupted.");
		} catch (InvalidFactoryFileException iffe) {
			iffe.printObjectTrace();
			throw new RuntimeException("Invalid Factory Game description received from server.");
		}
		
		if (!gameDescription.getRoles().containsKey(roleName)) {
			throw new IllegalArgumentException("Role '" + roleName + "' does not exist.");
		}
		
		role = gameDescription.getRoles().get(roleName);
		
		var controls = new ArrayList<String>();
		for (var resource : gameDescription.getResources().values()) {
			controls.add(FactoryGameDescription.placeName(resource.getName(), roleName, false, true));
			controls.add(FactoryGameDescription.placeName(resource.getName(), roleName, true, true));
		}

		petriGameClient = new PetriGameClient(url, controls, spacePrefix);
		
		serverSpace = PSpace.connect(url, spacePrefix + FactoryGameServer.RESOURCE_SPACE);
		store = new LinkedHashMap<>();
		stagingArea = new LinkedHashMap<>();
		updateResourcesSpace = new SequentialSpace();
		try {
			updateResourcesSpace.put(true);
		} catch (InterruptedException e) {
			throw new RuntimeException("Thread interrupted.");
		}
		updateResources();
		new Thread(() -> {
			while (true) {
				updateResources();
			}
		}).start();
	}
	
	/**
	 * @return A list of all recipes available for this client.
	 */
	public List<Recipe> getRecipes() {
		return role.getRecipes();
	}
	
	/**
	 * @return A map of resources and number of each in the store.
	 */
	public LinkedHashMap<Resource, Integer> getStore() {
		synchronized (store) {
			try {
				if (updateResourcesSpace.queryp(new ActualField(true)) == null)
					updateResourcesSpace.put(true);
			} catch (InterruptedException ie) {
				throw new RuntimeException("Thread interrupted.");
			}
			var result = new LinkedHashMap<Resource, Integer>();
			for (var resource : gameDescription.getResources().values()) {
				result.put(resource, store.get(resource));
			}
			return result;
		}
	}
	
	/**
	 * @return A map of resources and number of each in the staging area.
	 */
	public LinkedHashMap<Resource, Integer> getStagingArea() {
		synchronized (stagingArea) {
			try {
				if (updateResourcesSpace.queryp(new ActualField(true)) == null)
					updateResourcesSpace.put(true);
			} catch (InterruptedException ie) {
				throw new RuntimeException("Thread interrupted.");
			}
			var result = new LinkedHashMap<Resource, Integer>();
			for (var resource : gameDescription.getResources().values()) {
				result.put(resource, stagingArea.get(resource));
			}
			return result;
		}
	}
	
	/**
	 * @return A list of the resources the player has an infinite number of.
	 */
	public List<Resource> getInfiniteResources() {
		return new ArrayList<>(role.getInfiniteResources());
	}
	
	/**
	 * Moves one of the specified resource from the store to the staging area.
	 * If there are none of the specified resource in the store, then a RuntimeException is thrown.
	 * @param resource - The resource to move.
	 */
	public void stageResource(Resource resource) {
		stageResource(resource.getName());
	}

	/**
	 * Moves one of the specified resource from the store to the staging area.
	 * If there are none of the specified resource in the store, a RuntimeException is thrown.
	 * @param resourceName - The name of the resource to move.
	 */
	public void stageResource(String resourceName) {
		var resourceControlName = FactoryGameDescription.placeName(resourceName, roleName, false, true);
		petriGameClient.runMachine(resourceControlName);
	}
	
	/**
	 * Returns one of the specified resource from the staging area to the store.
	 * If there are none of the specified resource in the staging area, a RuntimeException is thrown.
	 * @param resource - The resource to move.
	 */
	public void unstageResource(Resource resource) {
		unstageResource(resource.getName());
	}
	
	/**
	 * Returns one of the specified resource from the staging area to the store.
	 * If there are none of the specified resource in the staging area, a RuntimeException is thrown.
	 * @param resource - The name of the resource to move.
	 */
	public void unstageResource(String resource) {
		var resourceControlName = FactoryGameDescription.placeName(resource, roleName, true, true);
		petriGameClient.runMachine(resourceControlName);
	}
	
	public Map<String, Integer> allTokens() {
		return petriGameClient.allTokens();
	}
	
	private void updateResources() {
		try {
			updateResourcesSpace.get(new ActualField(true));
		} catch (InterruptedException ie) {
			throw new RuntimeException("Thread interrupted.");
		}
		try {
			serverSpace.put(roleName);
			var response = (String)serverSpace.get(new ActualField(roleName), new FormalField(String.class))[1];
			var resourceStrings = response.split(";");
			var newStore = new LinkedHashMap<Resource, Integer>();
			var newStagingArea = new LinkedHashMap<Resource, Integer>();
			for (var resourceString : resourceStrings) {
				var values = resourceString.split(":");
				var storeValue = Integer.parseInt(values[1]);
				var stagingAreaValue = Integer.parseInt(values[2]);
				newStore.put(gameDescription.getResources().get(values[0]), storeValue);
				newStagingArea.put(gameDescription.getResources().get(values[0]), stagingAreaValue);
			}
			synchronized (store) {
				store = newStore;
			}
			synchronized (stagingArea) {
				stagingArea = newStagingArea;
			}
		} catch (InterruptedException ie) {
			throw new RuntimeException("Thread interrupted.");
		}
	}
}
