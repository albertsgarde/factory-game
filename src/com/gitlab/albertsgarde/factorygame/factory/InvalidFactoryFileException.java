package com.gitlab.albertsgarde.factorygame.factory;

import java.util.ArrayList;

public final class InvalidFactoryFileException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4792502183067045697L;
	
	private final ArrayList<String> parentMessages;
	
	public InvalidFactoryFileException(String message) {
		super(message);
		parentMessages = new ArrayList<>();
		parentMessages.add(message);
	}
	
	public void addParentMessage(String message) {
		parentMessages.add("at " + message);
	}
	
	public void printObjectTrace() {
		for (var message : parentMessages) {
			System.out.println(message);
		}
	}
}
