package com.gitlab.albertsgarde.factorygame.factory;

import java.io.IOException;

import com.google.gson.stream.JsonReader;

public class RecipeDestination {
	public final String role;
	public final boolean staged;
	
	public RecipeDestination (String role, boolean staged) {
		this.role = role;
		this.staged = staged;
	}
	
	public static RecipeDestination fromJson(JsonReader jsonReader) throws IOException, InvalidFactoryFileException {
		jsonReader.beginObject();
		String role = null;
		boolean staged = false;
		var foundStaged = false;
		while (jsonReader.hasNext()) {
			var key = jsonReader.nextName();
			switch (key) {
			case "role":
				if (role != null)
					throw new InvalidFactoryFileException("Multiple roles specified for recipe destination.");
				else {
					role = jsonReader.nextString();
				}
				break;
			case "staged":
				if (foundStaged)
					throw new InvalidFactoryFileException("Multiple values for staged specified for recipe destination.");
				else {
					staged = jsonReader.nextBoolean();
					foundStaged = true;
				}
				break;
			default:
				throw new InvalidFactoryFileException("Invalid field for recipe destination: '" + key + "'.");
			}
		}
		jsonReader.endObject();
		if (role == null)
			throw new InvalidFactoryFileException("No role specified for recipe destination.");
		if (!foundStaged)
			throw new InvalidFactoryFileException("Value 'staged' not specified for recipe destination.");
		return new RecipeDestination(role, staged);
	}
}
