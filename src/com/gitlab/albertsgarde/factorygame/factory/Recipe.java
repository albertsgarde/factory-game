package com.gitlab.albertsgarde.factorygame.factory;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.stream.JsonReader;

public final class Recipe {
	private final Map<Resource, Integer> inputs;
	private final Map<Resource, Integer> outputs;
	
	private final RecipeDestination destination;
	
	public Recipe(Map<Resource, Integer> inputs, Map<Resource, Integer> outputs, RecipeDestination destination) {
		this.inputs = inputs;
		this.outputs = outputs;
		this.destination = destination;
	}
	
	/**
	 * A map of resources and the amount required.
	 * @return The inputs of the recipe.
	 */
	public final Map<Resource, Integer> getInputs() {
		return inputs;
	}
	
	/**
	 * A map of resources and the amount produced by the recipe.
	 * @return The outputs of the recipe.
	 */
	public final Map<Resource, Integer> getOutputs() {
		return outputs;
	}
	
	public final RecipeDestination getDestination() {
		return destination;
	}
	
	public static Recipe fromJson(JsonReader jsonReader, Map<String, Resource> resourceMap) throws IOException, InvalidFactoryFileException {
		jsonReader.beginObject();
		LinkedHashMap<Resource, Integer> inputs = null;
		LinkedHashMap<Resource, Integer> outputs = null;
		RecipeDestination destination = null;
		while (jsonReader.hasNext()) {
			var key = jsonReader.nextName();
			switch (key) {
			case "inputs":
				if (inputs != null)
					throw new InvalidFactoryFileException("Multiple input lists defined for recipe.");
				else {
					try {
						inputs = FactoryGameDescription.resourceAmountList(jsonReader, resourceMap);
					} catch (InvalidFactoryFileException iffe) {
						iffe.addParentMessage("Recipe inputs");
						throw iffe;
					}
				}
				if (inputs.size() == 0) {
					throw new InvalidFactoryFileException("Recipes must take at least one input.");
				}
				break;
			case "outputs":
				if (outputs != null)
					throw new InvalidFactoryFileException("Multiple output lists defined for recipe.");
				else {
					try {
						outputs = FactoryGameDescription.resourceAmountList(jsonReader, resourceMap);
					} catch (InvalidFactoryFileException iffe) {
						iffe.addParentMessage("Recipe outputs");
						throw iffe;
					}
				}
				break;
			case "destination":
				if (destination != null)
					throw new InvalidFactoryFileException("Multiple destinations defined for recipe.");
				else {
					try {
						destination = RecipeDestination.fromJson(jsonReader);
					} catch (InvalidFactoryFileException iffe) {
						iffe.addParentMessage("Recipe destination");
						throw iffe;
					}
				}
				break;
			default:
				throw new InvalidFactoryFileException("Invalid field for recipe: '" + key + "'.");
			}
		}
		jsonReader.endObject();
		if (inputs == null)
			throw new InvalidFactoryFileException("No inputs defined for recipe.");
		if (outputs == null)
			throw new InvalidFactoryFileException("No outputs defined for recipe.");
		if (destination == null)
			throw new InvalidFactoryFileException("No destination defined for recipe.");
		return new Recipe(inputs, outputs, destination);
	}
}
