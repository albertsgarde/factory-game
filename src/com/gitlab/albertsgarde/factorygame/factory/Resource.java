package com.gitlab.albertsgarde.factorygame.factory;

import java.io.IOException;

import com.gitlab.albertsgarde.factorygame.graphics.Model;
import com.google.gson.stream.JsonReader;

public final class Resource {
	/** 
	 * The internal name of the resource.
	 */
	private final String name;
	/**
	 * The graphical model of the resource.
	 */
	private final Model model;
	
	public Resource(String name, Model model) {
		if (name.contains("/"))
			throw new IllegalArgumentException("A resource name may not contain a forward slash. Resource name: " + name);
		this.name = name;
		this.model = model;
	}
	
	/**
	 * @return The internal name of the resource.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The graphical model of the resource.
	 */
	public Model getModel() {
		return model;
	}

	/**
	 * Draws the resource's model so that the center of the model is at the given coordinates
	 * @param x - The x-coordinate of the center of the model.
	 * @param y - The y-coordinate of the center of the model.
	 * @param r - radius 
	 */
	public void draw(double x, double y , int r) {
		model.draw(x, y, r);
	}
	
	public static Resource fromJson(JsonReader jsonReader) throws IOException, InvalidFactoryFileException {
		jsonReader.beginObject();
		String name = null;
		Model model = null;
		while (jsonReader.hasNext()) {
			var key = jsonReader.nextName();
			if (key.equals("name")) {
				if (name != null)
					throw new InvalidFactoryFileException("Multiple names specified for resource.");
				else {
					name = jsonReader.nextString();
					if (name.contains("/"))
						throw new IllegalArgumentException("A resource name may not contain a forward slash. Resource name: " + name);
				}
			} else if (key.equals("model")) {
				if (model != null)
					throw new InvalidFactoryFileException("Multiple models defined for resource.");
				else {
					try {
						model = Model.fromJson(jsonReader);
					} catch (InvalidFactoryFileException iffe) {
						if (name != null)
							iffe.addParentMessage("Resource '" + name + "'");
						else 
							iffe.addParentMessage("Resource");
						throw iffe;
					}
				}
			} else {
				throw new InvalidFactoryFileException("Invalid field for resource: '" + key + "'.");
			}
		}
		jsonReader.endObject();
		if (name == null)
			throw new InvalidFactoryFileException("No name specified for resource.");
		if (model == null)
			throw new InvalidFactoryFileException("No model defined for resource.");
		return new Resource(name, model);
	}
}
