package com.gitlab.albertsgarde.factorygame.factory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.gitlab.albertsgarde.factorygame.utils.Utils;
import com.google.gson.stream.JsonReader;

public class Role {
	private final Map<Resource, Integer> initialResources;
	
	private final Set<Resource> infiniteResources;
	
	private final Set<String> infiniteResourceNames;
	
	private final List<Recipe> recipes;

	public Role(Map<Resource, Integer> initialResources, Set<Resource> infiniteResources, List<Recipe> recipes) {
		this.initialResources = Utils.toOwned(initialResources);
		this.infiniteResources = Utils.toOwnedSet(infiniteResources);
		this.recipes = Utils.toOwned(recipes);
		var infiniteResourceNames = new LinkedHashSet<String>();
		for (var resource : infiniteResources) {
			infiniteResourceNames.add(resource.getName());
		}
		this.infiniteResourceNames = infiniteResourceNames;
	}

	public Map<Resource, Integer> getInitialResources() {
		return Collections.unmodifiableMap(initialResources);
	}
	
	public Set<Resource> getInfiniteResources() {
		return Collections.unmodifiableSet(infiniteResources);
	}
	
	public Set<String> getInfiniteResourceNames() {
		return Collections.unmodifiableSet(infiniteResourceNames);
	}

	public List<Recipe> getRecipes() {
		return Collections.unmodifiableList(recipes);
	}
	
	public static Role fromJson(JsonReader jsonReader, Map<String, Resource> resourceMap) throws IOException, InvalidFactoryFileException {
		jsonReader.beginObject();
		LinkedHashMap<Resource, Integer> initialResources = null;
		Set<Resource> infiniteResources = null;
		List<Recipe> recipes = null;
		while (jsonReader.hasNext()) {
			var key = jsonReader.nextName();
			switch (key) {
			case "startingResources":
				if (initialResources != null) {
					throw new InvalidFactoryFileException("Multiple starting resource lists defined for player.");
				}
				try {
					initialResources = FactoryGameDescription.resourceAmountList(jsonReader, resourceMap);
				} catch (InvalidFactoryFileException iffe) {
					iffe.addParentMessage("Player starting resources");
					throw iffe;
				}
				break;
			case "infiniteResources":
				if (infiniteResources != null) {
					throw new InvalidFactoryFileException("Multiple infinite resource lists defined for player.");
				}
				try {
					infiniteResources = new LinkedHashSet<Resource>();
					jsonReader.beginArray();
					while (jsonReader.hasNext()) {
						var resourceName = jsonReader.nextString();
						if (!resourceMap.containsKey(resourceName)) {
							throw new InvalidFactoryFileException("Resource '" + resourceName + "' specified in infinite resource list does not exist.");
						}
						var resource = resourceMap.get(resourceName);
						if (infiniteResources.contains(resource)) {
							throw new InvalidFactoryFileException("Resource '" + resourceName + "' specified twice as an infinite resource.");
						}
						infiniteResources.add(resource);
					}
					jsonReader.endArray();
				} catch (InvalidFactoryFileException iffe) {
					iffe.addParentMessage("Player starting resources");
					throw iffe;
				}
				break;
			case "recipes":
				if (recipes != null) {
					throw new InvalidFactoryFileException("Multiple recipe lists defined for player.");
				}
				try {
					recipes = recipeListFromJson(jsonReader, resourceMap);
				} catch (InvalidFactoryFileException iffe) {
					iffe.addParentMessage("Player recipe list");
					throw iffe;
				}
				break;
			default:
				throw new InvalidFactoryFileException("Invalid field for player: '" + key + "'.");
			}
		}
		jsonReader.endObject();
		if (initialResources == null)
			throw new InvalidFactoryFileException("No starting resource list defined for player.");
		if (infiniteResources == null)
			throw new InvalidFactoryFileException("No infinite resource list defined for player.");
		if (recipes == null)
			throw new InvalidFactoryFileException("No recipe list defined for player.");
		
		for (var infiniteResource : infiniteResources) {
			if (initialResources.containsKey(infiniteResource) && initialResources.get(infiniteResource) != 0)
				throw new InvalidFactoryFileException("Resource '" + infiniteResource + "' defined as both an infinite and a starting resource.");
		}
		
		return new Role(initialResources, infiniteResources, recipes);
	}
	
	private static List<Recipe> recipeListFromJson(JsonReader jsonReader, Map<String, Resource> resourceMap) throws IOException, InvalidFactoryFileException {
		jsonReader.beginArray();
		var result = new ArrayList<Recipe>();
		var recipeNum = 0;
		while (jsonReader.hasNext()) {
			try {
				var recipe = Recipe.fromJson(jsonReader, resourceMap);
				result.add(recipe);
			} catch (InvalidFactoryFileException iffe) {
				iffe.addParentMessage("Recipe number " + recipeNum);
				throw iffe;
			}
			++recipeNum;
		}
		jsonReader.endArray();
		return result;
	}
}
