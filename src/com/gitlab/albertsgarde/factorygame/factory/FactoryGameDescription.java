package com.gitlab.albertsgarde.factorygame.factory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.gitlab.albertsgarde.factorygame.petrigame.PetriGameDescription;
import com.gitlab.albertsgarde.factorygame.petrispace.PetriNet;
import com.gitlab.albertsgarde.factorygame.petrispace.PostFlow;
import com.gitlab.albertsgarde.factorygame.petrispace.PreFlow;
import com.gitlab.albertsgarde.factorygame.petrispace.Transition;
import com.gitlab.albertsgarde.factorygame.utils.Utils;
import com.google.gson.stream.JsonReader;

public final class FactoryGameDescription {
	private final LinkedHashMap<String, Resource> resources;
	
	private final Map<String, Role> roles;
	
	public FactoryGameDescription(List<Resource> resources, Map<String, Role> roles) {
		this.resources = new LinkedHashMap<>();
		for (var resource : resources) {
			this.resources.put(resource.getName(), resource);
		}
		this.roles = Utils.toOwned(roles);
	}
	
	/**
	 * @return A list of the resources present in this map.
	 */
	public LinkedHashMap<String, Resource> getResources() {
		return resources;
	}

	/**
	 * @return A list of all roles.
	 */
	public Map<String, Role> getRoles() {
		return Collections.unmodifiableMap(roles);
	}
	
	/**
	 * Creates a PetriGameDescription for this Factory Game map.
	 * @return The created PetriGameDescription.
	 */
	PetriGameDescription toPetriGame() {
		var initialMarking = new HashMap<String, Integer>();
		for (var role : roles.entrySet()) {
			for (var resource : resources.values()) {
				if (role.getValue().getInitialResources().containsKey(resource)) {
					initialMarking.put(placeName(resource.getName(), role.getKey(), false, false), role.getValue().getInitialResources().get(resource));
				} else {
					initialMarking.put(placeName(resource.getName(), role.getKey(), false, false), 0);
				}
				initialMarking.put(placeName(resource.getName(), role.getKey(), true, false), 0);
				initialMarking.put(placeName(resource.getName(), role.getKey(), false, true), 0);
				initialMarking.put(placeName(resource.getName(), role.getKey(), true, true), 0);
			}
		}
		
		var transitions = new HashMap<String, Transition>();
		var controls = new HashMap<String, String>();
		
		for (var role : roles.entrySet()) {
			for (var resource : resources.values()) {
				var infinite = role.getValue().getInfiniteResources().contains(resource);
				// Player 1 resource stage.
				{
					var preFlows = new ArrayList<PreFlow>();
					preFlows.add(new PreFlow(placeName(resource.getName(), role.getKey(), false, true), PreFlow.Type.CONSUME, 1));
					if (!infinite) {
						preFlows.add(new PreFlow(placeName(resource.getName(), role.getKey(), false, false), PreFlow.Type.CONSUME, 1));
					}
					var postFlows = Arrays.asList(new PostFlow(placeName(resource.getName(), role.getKey(), true, false), 1));
					var transition = new Transition(preFlows, postFlows);
					var transitionName = placeName(resource.getName(), role.getKey(), false, false);
					transitions.put(transitionName, transition);
				}
				// Player 1 resource stage control.
				{
					var preFlows = new ArrayList<PreFlow>();
					var postFlows = Arrays.asList(new PostFlow(placeName(resource.getName(), role.getKey(), false, true), 1));
					var transition = new Transition(preFlows, postFlows);
					var transitionName = placeName(resource.getName(), role.getKey(), false, true);
					transitions.put(transitionName, transition);
					controls.put(transitionName, transitionName);
				}
				// Player 1 resource unstage.
				{
					var preFlows = Arrays.asList(new PreFlow(placeName(resource.getName(), role.getKey(), true, false), PreFlow.Type.CONSUME, 1),
							 					 new PreFlow(placeName(resource.getName(), role.getKey(), true, true), PreFlow.Type.CONSUME, 1));
					var postFlows = infinite ? new ArrayList<PostFlow>() :
							Arrays.asList(new PostFlow(placeName(resource.getName(), role.getKey(), false, false), 1));
					var transition = new Transition(preFlows, postFlows);
					var transitionName = placeName(resource.getName(), role.getKey(), true, false);
					transitions.put(transitionName, transition);
				}
				// Player 1 resource unstage control.
				{
					var preFlows = new ArrayList<PreFlow>();
					var postFlows = Arrays.asList(new PostFlow(placeName(resource.getName(), role.getKey(), true, true), 1));
					var transition = new Transition(preFlows, postFlows);
					var transitionName = placeName(resource.getName(), role.getKey(), true, true);
					transitions.put(transitionName, transition);
					controls.put(transitionName, transitionName);
				}
			}
		}
		
		// Recipe transitions.
		var recipeNum = 0;
		for (var role : roles.entrySet()) {
			for (var recipe : role.getValue().getRecipes()) {
				var preFlows = new ArrayList<PreFlow>();
				for (var ingredient : recipe.getInputs().entrySet()) {
					var placeName = placeName(ingredient.getKey().getName(), role.getKey(), true, false);
					preFlows.add(new PreFlow(placeName, PreFlow.Type.CONSUME, ingredient.getValue()));
				}
				var postFlows = new ArrayList<PostFlow>();
				for (var ingredient : recipe.getOutputs().entrySet()) {
					var placeName = placeName(ingredient.getKey().getName(), recipe.getDestination().role, recipe.getDestination().staged, false);
					postFlows.add(new PostFlow(placeName, ingredient.getValue()));
				}
				transitions.put(recipeNum + "R", new Transition(preFlows, postFlows));
				++recipeNum;
			}
			
		}
		
		var petriNet = new PetriNet(transitions);
		return new PetriGameDescription(petriNet, controls, initialMarking);
	}
	
	/**
	 * 
	 * @param resourceName
	 * @param player
	 * @param staging - From or in staging area.
	 * @param control
	 * @return
	 */
	static String placeName(String resourceName, String roleName, boolean staging, boolean control) {
		return resourceName + "/" + roleName + (staging ? "S" : "_") + (control ? "C" : "_");
	}
	
	public static FactoryGameDescription fromJson(JsonReader jsonReader) throws IOException, InvalidFactoryFileException {
		jsonReader.beginObject();
		// Resources.
		LinkedHashMap<String, Resource> resources = null;
		if (!jsonReader.nextName().equals("resources")) {
			throw new InvalidFactoryFileException("First element must be the resource list.");
		}
		try {
			resources = resourceListFromJson(jsonReader);
		} catch (InvalidFactoryFileException iffe) {
			iffe.addParentMessage("Map resource list");
			throw iffe;
		}
		
		// Roles.
		var roles = new LinkedHashMap<String, Role>();
		if (!jsonReader.nextName().equals("roles"))
			throw new InvalidFactoryFileException("Second element must be the role list.");
		jsonReader.beginObject();
		while (jsonReader.hasNext()) {
			var roleName = jsonReader.nextName();
			if (roleName.contains("/"))
				throw new InvalidFactoryFileException("Role name may not contain forward slashes. Role name: " + roleName);
			if (roles.containsKey(roleName)) {
				throw new InvalidFactoryFileException("Multiple roles with same name not allowed. Name: " + roleName);
			}
			try {
				var role = Role.fromJson(jsonReader, resources);
				roles.put(roleName, role);
			} catch (InvalidFactoryFileException iffe) {
				iffe.addParentMessage("Role '" + roleName + "'");
				throw iffe;
			}
		}
		jsonReader.endObject();
		
		// Check for recipes giving roles their infinite resources.
		for (var roleEntry : roles.entrySet()) {
			var role = roleEntry.getValue();
			var roleName = roleEntry.getKey();
			for (var recipe : role.getRecipes()) {
				if (!recipe.getDestination().staged) {
					var destRoleName = recipe.getDestination().role;
					var destRole = roles.get(destRoleName);
					for (var resource : recipe.getOutputs().keySet()) {
						if (destRole.getInfiniteResources().contains(resource)) {
							throw new InvalidFactoryFileException("Recipe for role '" + roleName + "' outputs resource '" + 
									resource.getName() + "' to store of role '" + destRoleName + "'. They already have infinite of this resource.");
						}
					}
				}
			}
		}
		
		jsonReader.endObject();
		return new FactoryGameDescription(new ArrayList<Resource>(resources.values()), roles);
	}
	
	static LinkedHashMap<Resource, Integer> resourceAmountList(JsonReader jsonReader, Map<String, Resource> resourceMap) throws IOException, InvalidFactoryFileException {
		jsonReader.beginObject();
		var resourceList = new LinkedHashMap<Resource, Integer>();
		int resourceNum = 0;
		while (jsonReader.hasNext()) {
			var key = jsonReader.nextName();
			if (!resourceMap.containsKey(key)) {
				throw new InvalidFactoryFileException("Invalid resource specified: '" + key + "' at resource number " + resourceNum + ".");
			}
			var resource = resourceMap.get(key);
			if (resourceList.containsKey(resource)) {
				throw new InvalidFactoryFileException("Resource '" + key + "' specified multiple times at resource number " + resourceNum + ".");
			}
			var amount = jsonReader.nextInt();
			if (amount < 0) {
				throw new InvalidFactoryFileException("Negative resource amounts may not be specified. Resource number: " + resourceNum + ". Amount: '" + amount + "'");
			}
			resourceList.put(resource, amount);
			++resourceNum;
		}
		jsonReader.endObject();
		return resourceList;
	}
	
	private static LinkedHashMap<String, Resource> resourceListFromJson(JsonReader jsonReader) throws IOException, InvalidFactoryFileException {
		jsonReader.beginArray();
		var result = new LinkedHashMap<String, Resource>();
		var resourceNum = 0;
		while (jsonReader.hasNext()) {
			try {
				var resource = Resource.fromJson(jsonReader);
				result.put(resource.getName(), resource);
			} catch (InvalidFactoryFileException iffe) {
				iffe.addParentMessage("Resource number " + resourceNum);
				throw iffe;
			}
			++resourceNum;
		}
		jsonReader.endArray();
		return result;
	}
}
