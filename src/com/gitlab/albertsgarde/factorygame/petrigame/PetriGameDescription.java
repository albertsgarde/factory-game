package com.gitlab.albertsgarde.factorygame.petrigame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.gitlab.albertsgarde.factorygame.petrispace.PetriNet;
import com.gitlab.albertsgarde.factorygame.utils.Utils;

public final class PetriGameDescription {
	private final PetriNet petriNet;
	
	private final List<String> serverMachines;
	
	private final Map<String, String> clientControls;
	
	private final Map<String, Integer> initialMarking;
	
	public PetriGameDescription(PetriNet petriNet, Map<String, String> controls, Map<String, Integer> initialMarking) {
		this.petriNet = petriNet;
		this.initialMarking = Utils.toOwned(initialMarking);
		this.clientControls = controls;
		serverMachines = new ArrayList<>();
		for (var transition : petriNet.getTransitions().entrySet()) {
			if (!clientControls.containsValue(transition.getKey()))
				serverMachines.add(transition.getKey());
		}
	}

	PetriNet getPetriNet() {
		return petriNet;
	}

	List<String> getServerMachines() {
		return Collections.unmodifiableList(serverMachines);
	}
	
	Map<String, String> getControls() {
		return Collections.unmodifiableMap(clientControls);
	}
	
	String getControl(String control) {
		return clientControls.get(control);
	}
	
	Map<String, Integer> getInitialMarking() {
		return Collections.unmodifiableMap(initialMarking);
	}
}
