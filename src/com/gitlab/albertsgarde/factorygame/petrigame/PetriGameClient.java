package com.gitlab.albertsgarde.factorygame.petrigame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jspace.ActualField;
import org.jspace.RemoteSpace;
import org.jspace.SequentialSpace;
import org.jspace.Space;

import com.gitlab.albertsgarde.factorygame.petrispace.PetriSpace;
import com.gitlab.albertsgarde.factorygame.utils.PSpace;

public class PetriGameClient {
	private static final String WAITING_CODE = "AWAIT";
	
	private final PetriSpace petriSpace;
	
	private final Space controlSpace;
	
	private final List<Thread> controlThreads;
	
	public PetriGameClient(String url, List<String> controls, String spacePrefix) {
		petriSpace = PetriSpace.connectToPetriSpace(url, spacePrefix);
		controlThreads = new ArrayList<Thread>();
		controlSpace = new SequentialSpace();
		RemoteSpace serverControlSpace;
		try {
			serverControlSpace = PSpace.connect(url, spacePrefix + PetriGameServer.CONTROL_SPACE);
		} catch (IOException e1) {
			throw new RuntimeException("Could not connect to server.");
		}
		for (var controlName : controls) {
			var thread = new Thread(() -> {
				while (true) {
					try {
						serverControlSpace.get(new ActualField(controlName), new ActualField(false));
						control(controlSpace, controlName);
						serverControlSpace.put(controlName, true);
					} catch (InterruptedException ie) {
						throw new RuntimeException("Thread interrupted.");
					}
				}
			});
			thread.start();
			controlThreads.add(thread);
		}
	}
	
	/**
	 * If the specified machine is waiting for a control signal, fire it.
	 * Otherwise do nothing.
	 * @param machineName - The name of the machine to fire.
	 */
	public void runMachine(String machineName) {
		try {
			if (controlSpace.getp(new ActualField(WAITING_CODE), new ActualField(machineName)) != null) {
				controlSpace.put(machineName);
			}
		} catch (InterruptedException e) {
			throw new RuntimeException("Thread interrupted.");
		}
	}
	
	public Map<String, Integer> allTokens() {
		return petriSpace.allTokens();
	}
	
	public void shutDown() {
		// TODO: Shut down more gracefully
		for (var thread : controlThreads) {
			thread.interrupt();
		}
		for (var thread : controlThreads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				throw new RuntimeException("Interrupted while waiting for threads to finish.");
			}
		}
	}
	
	private void control(Space controlSpace, String controlName) {
		try {
			controlSpace.put(WAITING_CODE, controlName);
			controlSpace.get(new ActualField(controlName));
		} catch (InterruptedException e) {
			throw new RuntimeException("Thread interrupted.");
		}
	}
}
