package com.gitlab.albertsgarde.factorygame.petrigame;

import java.util.ArrayList;
import java.util.List;

import org.jspace.ActualField;
import org.jspace.SequentialSpace;
import org.jspace.SpaceRepository;

import com.gitlab.albertsgarde.factorygame.petrispace.PetriSpace;

public final class PetriGameServer {
	private static final String URL_CODE = "BCm5VPOZ4U23DdsLGPA8v1lc";
	static final String CONTROL_SPACE = "petriGameControls" + URL_CODE;
	
	private final PetriSpace petriSpace;
	
	private final List<Thread> machineThreads;
	
	public PetriGameServer(PetriGameDescription gameDescription, SpaceRepository repository, String spacePrefix) {
		repository.add(spacePrefix + CONTROL_SPACE, new SequentialSpace());
		
		petriSpace = PetriSpace.createPetriSpace(repository, gameDescription.getPetriNet(), gameDescription.getInitialMarking(), spacePrefix);
		
		machineThreads = new ArrayList<>();
		for (var machineID : gameDescription.getServerMachines()) {
			machineThreads.add(petriSpace.startTransitionOnNewThread(machineID, () -> {}));
		}
		var controlSpace = repository.get(spacePrefix + CONTROL_SPACE);
		for (var control : gameDescription.getControls().entrySet()) {
			machineThreads.add(petriSpace.startTransitionOnNewThread(control.getValue(), () -> {
				try {
					controlSpace.put(control.getKey(), false);
					controlSpace.get(new ActualField(control.getKey()), new ActualField(true));
				} catch (InterruptedException ie) {
					throw new RuntimeException("Thread interrupted.");
				}
			}));
		}
	}
	
	public void shutDown() {
		// TODO: Shut down more gracefully
		for (var thread : machineThreads) {
			thread.interrupt();
		}
		join();
	}
	
	public void join() {
		for (var thread : machineThreads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				throw new RuntimeException("Interrupted while waiting for threads to finish.");
			}
		}
	}
	
	/**
	 * @param place - The place whose number of tokens to return.
	 * @return The number of tokens in the specified place.
	 */
	public int numTokens(String place) {
		return petriSpace.numTokens(place);
	}
}
