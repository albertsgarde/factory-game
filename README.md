# Factory Game
A game where players work together to complete recipes in order to produce points.

The game is built on top of a more general engine which in essence gamifies a Petri net by selectively allowing players control over token placement.

# Word list

## Factory Game
The end result of the project.
This is what players experience when running the executables and will have a GUI.
Built on the Petri Game engine.

## Petri Game
A more general version of Factory Game very closely inspired by a Petri net.
Only exists as an engine on which Factory Game is built.
This however means that it wouldn't be too difficult to create other games based on the same design.
Built on a Petri Space.

## Petri Space
An object that allows usage of a Petri net without having to worry about any details.
Only describing the net and giving functions for the transitions is necessary.
A Petri Space uses pspaces for its implementation, but the user never sees this and shouldn't try to interact with them manually other than through the public interface a Petri Space provides.
