# Gui-Logic interface

## Logic -> Gui
Number of each resource in store.
Number of each resource in staging area.
All available recipes.

## Gui -> Logic
Move resource from store to staging area.
Move resource from staging area to store.
